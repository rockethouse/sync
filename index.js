import busboyBodyParser from 'busboy-body-parser'
import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import morgan from 'morgan'
import passport from 'passport'
import Raven from 'raven'
import schedule from 'node-schedule'
import winston from 'winston'
import models from './models'
import routes from './routes'
import actions from './actions'

winston.info('NODE_ENV: ' + process.env.NODE_ENV)
if (process.env.NODE_ENV === 'production') {
  winston.info('Initializing Raven...')
  Raven
    .config('https://e6243302f3cc4a00afb9fdd02e61b0b7:18344d5b25f24657913c6502e023a621@sentry.io/126066')
    .install()
}

const app = express()

app.use('/', express.static('public'))

app.use(cors({
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  preflightContinue: false,
  allowedHeaders: 'Authorization, Content-Type, Depth, User-Agent, Cache-Control'
}))

app.use(busboyBodyParser({ limit: '10mb' }))
app.use(bodyParser.json({ limit: '10mb' }))
app.use(bodyParser.urlencoded({ extended: true }))

app.use(morgan('short', winston.stream))

app.use(passport.initialize())

app.use('/api/', routes)

models.sequelize.sync().then(() => {
  winston.info('started!')
  schedule.scheduleJob('5-55/5 * * * *', () => { actions.partialRefreshAndPush() })
  schedule.scheduleJob('0 */1 * * *', () => { actions.fullRefreshAndPush() })
  actions.fullRefreshAndPush()
  app.listen(process.env.HTTP_PORT || 8181)
})
