import { Router } from 'express'
import actions from './actions'

const router = new Router()

router.get('/', (req, res) => {
  res.send('ok')
})

router.get('/update/partial', (req, res) => {
  actions.partialRefreshAndPush()
  res.send('ok')
})

router.get('/update/full', (req, res) => {
  actions.fullRefreshAndPush()
  res.send('ok')
})

router.get('/update/date/:date', (req, res) => {
  actions.fetchTimeEntriesFromTimecamp(req.param.date, () => {
    res.send('ok!')
  })
})

export default router
