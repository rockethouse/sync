import async from 'async'
import request from 'request'
import rp from 'request-promise-native'
import moment from 'moment-timezone'
import winston from 'winston'
import models from './models'
import config from './config.json'

export default {
  fetchTasksFromTimecamp(done) {
    winston.info('Fetching Tasks from Timecamp')
    request(`https://www.timecamp.com/third_party/api/tasks/api_token/${config.timecamp.apikey}/format/json`, (error, response, body) => {
      const tasks = JSON.parse(body)
      models.sequelize.transaction((transaction) => {
        return Promise.all(
          Object.keys(tasks).map((task) => {
          return models.TimecampTask.upsert(tasks[task],{ transaction })
            .catch((error) => {
              winston.error(error)
            })
          })
        )
      })
      .then(() => {
        done()
      })
      .catch((error) => {
        done(error)
      })
    })
  },

  fetchTimeEntriesFromTimecampWrapper(done) {
    this.fetchTimeEntriesFromTimecamp(null, done)
  },

  fetchTimeEntriesFromTimecamp(date, done) {
    winston.info('Fetching TimeEntries from Timecamp')
    const dateFormatted = moment(date).format('YYYY-MM-DD')
    request(`https://www.timecamp.com/third_party/api/entries/api_token/${config.timecamp.apikey}/from/${dateFormatted}/to/${dateFormatted}/format/json`, (error, response, body) => {
      const entries = JSON.parse(body)
      models.sequelize.transaction((transaction) => {
        return Promise.all(
          entries.map((entry) => {
            return models.TimecampEntry.upsert(entry, { transaction })
          })
        )
      })
      .then(() => {
        done()
      })
      .catch((error) => {
        done(error)
      })
    })
  },

  fetchUsersfromTimecamp(done) {
    winston.info('Fetching Users from Timecamp')
    request(`https://www.timecamp.com/third_party/api/users/api_token/${config.timecamp.apikey}/format/json`, (error, response, body) => {
      const users = JSON.parse(body)
      models.sequelize.transaction((transaction) => {
        return Promise.all(
          users.map((user) => {
            return models.TimecampUser.upsert(user, { transaction })
          })
        )
      })
      .then(() => done())
      .catch((error) => done(error))
    })
  },

  fetchProjectsFromHarvest(done) {
    winston.info('Fetching Projects from Harvest')
    request(`https://${config.harvest.subdomain}.harvestapp.com/projects`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }, (error, response, body) => {
      const projects = JSON.parse(body)
      models.sequelize.transaction((transaction) => {
        return Promise.all(
          projects.map((project) => {
            return models.HarvestProject.upsert(project.project, { transaction })
          })
        )
      })
      .then(() => done())
      .catch((error) => done(error))
    }).auth(config.harvest.username, config.harvest.password)
  },

  fetchUsersFromHarvest(done) {
    winston.info('Fetching users from Harvest')
    request(`https://${config.harvest.subdomain}.harvestapp.com/people`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }, (error, response, body) => {
      const users = JSON.parse(body)
      models.sequelize.transaction((transaction) => {
        return Promise.all(
          users.map((user) => {
            return models.HarvestUser.upsert(user.user, { transaction })
          })
        )
      })
      .then(() => done())
      .catch((error) => done(error))
    }).auth(config.harvest.username, config.harvest.password)
  },

  publishTimecampTasksToHarvest(done) {
    if (process.env.NODE_ENV !== 'production') {
      winston.info('!!SKIPPED!! Publishing Timecamp Tasks to Harvest')
      done()
      return
    }
    winston.info('Publishing Timecamp Tasks to Harvest')
    models.sequelize.query(`
      SELECT HU.id as 'HarvestUserID', HU.email, TT.task_id, TT.name, TT.tags, HP.id as 'HarvestProjectID', HP.code, HP.active, TE.* FROM TimecampEntries TE
      LEFT JOIN TimecampUsers TU ON TU.user_id = TE.user_id
      LEFT JOIN HarvestUsers HU ON TU.email = HU.email
      LEFT JOIN TimecampTasks TT ON TE.task_id = TT.task_id
      LEFT JOIN HarvestProjects HP ON INSTR(TT.tags, HP.code) = 1
      WHERE code <> '' AND HP.active = 1 AND TE.exported = 0`
    ).then((rows) => {
      return Promise.all(
        rows[0].map((row) => {
          const data = {
            notes: `Automatically imported from TimeCamp on ${moment().format('YYYY-MM-DD')}`,
            hours: Math.max(parseFloat((row.duration / 3600)).toFixed(2), 0.01),
            project_id: row.HarvestProjectID + '',
            task_id: 2256538 + '', // "Web Development"
            spent_at: moment(row.date, 'YYYY-MM-DD HH:mm:ss.SSS ZZ').clone().tz('Australia/Adelaide').format('YYYY-MM-DD')
          }
          winston.info('Posting data to Harvest:' + JSON.stringify(data))
          return rp({
            method: 'POST',
            url: `https://${config.harvest.subdomain}.harvestapp.com/daily/add?of_user=${row.HarvestUserID}`,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            auth: {
              username: config.harvest.username,
              password: config.harvest.password
            },
            json: data
          }).then(() => {
            winston.info(`${row.name} ${row.duration} ${(row.duration/3600)}`)
            return row.task_id
          })
          .catch((error) => {
            winston.error(`Error publishing Timecamp Tasks to Harvest: ${error}`)
          })
        })
      )
    })
    .then((updatedIds) => {
      updatedIds = updatedIds.filter(taskId => !!taskId)
      winston.info(updatedIds)
      models.sequelize.transaction((transaction) => {
        return Promise.all(
          updatedIds.map((taskId) => {
            return models.TimecampEntry.update({ exported: true }, { where: { task_id: taskId }, transaction })
          })
        )
      })
      done()
    })
    .catch((error) => {
      winston.error(error)
    })
  },

  fullRefreshAndPush() {
    winston.info('Performing full refresh and push.')
    async.series([
      this.fetchUsersfromTimecamp,
      this.fetchTasksFromTimecamp,
      this.fetchTimeEntriesFromTimecampWrapper,
      this.fetchProjectsFromHarvest,
      this.fetchUsersFromHarvest,
      this.publishTimecampTasksToHarvest
    ], (error) => {
      if (error) {
        winston.error(error)
      }
      winston.info('done with full refresh!')
    })
  },

  partialRefreshAndPush() {
    winston.info('Performing partial refresh and push.')
    async.series([
      this.fetchTimeEntriesFromTimecampWrapper,
      this.publishTimecampTasksToHarvest
    ], () => {
      winston.info('done with partial refresh!')
    })
  }
}
