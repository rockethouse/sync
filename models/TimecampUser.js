export default (sequelize, DataTypes) => {
  return sequelize.define('TimecampUser', {
    group_id: DataTypes.INTEGER,
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    email: DataTypes.STRING,
    login_count: DataTypes.INTEGER,
    login_time: DataTypes.DATE,
    display_name: DataTypes.STRING,
    synch_time: DataTypes.DATE,
  })
}
