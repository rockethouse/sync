export default (sequelize, DataTypes) => {
  const TimecampEntry = sequelize.define('TimecampEntry', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    duration: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    user_name: DataTypes.STRING,
    description: DataTypes.STRING,
    task_id: DataTypes.INTEGER,
    date: DataTypes.DATE,
    start_time: DataTypes.STRING,
    end_time: DataTypes.STRING,
    locked: DataTypes.INTEGER,
    name: DataTypes.STRING,
    addons_external_id: DataTypes.INTEGER,
    billable: DataTypes.INTEGER,
    invoiceId: DataTypes.INTEGER,
    exported: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
  {
    classMethods: {
      associate(models) {
        TimecampEntry.belongsTo(models.TimecampTask, { foreignKey: 'task_id' })
        TimecampEntry.belongsTo(models.TimecampUser, { foreignKey: 'user_id' })
      }
    }
  })
  return TimecampEntry
}
