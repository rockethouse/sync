export default (sequelize, DataTypes) => {
  return sequelize.define('HarvestProject', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    client_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    billable: DataTypes.BOOLEAN,
    bill_by: DataTypes.STRING,
    budget: DataTypes.INTEGER,
    budget_by: DataTypes.STRING,
    notify_when_over_budget: DataTypes.BOOLEAN,
    over_budget_notification_percentage: DataTypes.INTEGER,
    over_budget_notified_at: DataTypes.DATE,
    show_budget_to_all: DataTypes.BOOLEAN,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    starts_on: DataTypes.DATE,
    ends_on: DataTypes.DATE,
    estimate: DataTypes.INTEGER,
    estimate_by: DataTypes.STRING,
    hint_earliest_record_at: DataTypes.DATE,
    hint_latest_record_at: DataTypes.DATE,
    notes: DataTypes.STRING,
    hourly_rate: DataTypes.INTEGER,
    cost_budget: DataTypes.INTEGER,
    cost_budget_include_expenses: DataTypes.BOOLEAN
  })
}
