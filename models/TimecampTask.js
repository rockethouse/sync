export default (sequelize, DataTypes) => {
  const TimecampTask =  sequelize.define('TimecampTask', {
    task_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      validate: {
        isInt: true
      }
    },
    parent_id: DataTypes.INTEGER,
    assigned_by: DataTypes.INTEGER,
    name: DataTypes.STRING,
    external_task_id: DataTypes.INTEGER,
    external_parent_id: DataTypes.INTEGER,
    level: DataTypes.INTEGER,
    archived: DataTypes.INTEGER,
    color: DataTypes.STRING,
    tags: DataTypes.STRING,
    budgeted: DataTypes.INTEGER,
    budget_unit: DataTypes.STRING,
    root_group_id: DataTypes.INTEGER,
    billable: DataTypes.INTEGER,
    user_access_type: DataTypes.INTEGER
  },
  {
    classMethods: {
      associate(models) {
        TimecampTask.hasMany(models.TimecampEntry, { foreignKey: 'task_id' })
      }
    }
  })
  return TimecampTask
}
