export default (sequelize, DataTypes) => {
  return sequelize.define('HarvestUser', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    email: DataTypes.STRING,
    created_at: DataTypes.DATE,
    is_admin: DataTypes.BOOLEAN,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    timezone: DataTypes.STRING,
    is_contractor: DataTypes.BOOLEAN,
    telephone: DataTypes.STRING,
    is_active: DataTypes.BOOLEAN,
    has_access_to_all_future_projects: DataTypes.BOOLEAN,
    default_hourly_rate: DataTypes.INTEGER,
    department: DataTypes.STRING,
    wants_newsletter: DataTypes.BOOLEAN,
    updated_at: DataTypes.DATE,
    cost_rate: DataTypes.INTEGER,
    weekly_capacity: DataTypes.INTEGER,
  })
}
