import fs from 'fs'
import path from 'path'
import Sequelize from 'sequelize'

const sequelize = new Sequelize('rockethouse', null, null, {
  dialect: 'sqlite',
  storage: 'storage/database.sqlite',
  logging: false
})

const db = {
  sequelize,
  Sequelize
}

fs.readdirSync(__dirname)
  .filter((file) => {
    return (file.indexOf('.') !== 0) && (file !== 'index.js')
  })
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db)
  }
})


export default db
